from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import sqlite3
import io

# base de datos

def get_contactos():
	conexion = sqlite3.connect('contactos.db')
	cursor = conexion.cursor()
	cursor.execute("SELECT * FROM contactos")
	contactos = cursor.fetchall()
	return contactos

def fetch_contacto( id ):
	conexion = sqlite3.connect('contactos.db')
	cursor = conexion.cursor()
	args= (id,)
	cursor.execute("SELECT * FROM contactos WHERE id = ?", args)
	contacto = cursor.fetchone()
	conexion.close()
	return contacto
	
def print_contactos():
	contactos = get_contactos()
	for c in contactos:
		contacto = {"id": c[0], "nombre": c[1], "telefono": c[2], "correo": c[3],}
		Label(ventana, text=contacto.get("nombre")).grid(column=1, row=contacto.get("id"))
		Label(ventana, text=contacto.get("correo")).grid(column=2, row=contacto.get("id"))
		Button(ventana, text="Editar" , command=lambda: editar_contacto( contacto.get("id") ) ).grid( column=3, row=contacto.get("id") )


def export_contactos():
	texto = ""
	contactos = get_contactos()
	for c in contactos:
		texto += '|| '+c[1] + ' | ' + c[2] + ' | ' + c[3] + ' ||\n'
	path = filedialog.asksaveasfilename(title="Exportar Archivo", initialdir=".")
	documento = open(path, 'w')
	documento.write(texto)
	documento.close()
	messagebox.showinfo('Exportar', 'Se exportó con exito la lista de contactos')





# Funciones

def editar_contacto(indice):
	def limpiar_form():
		nombre.set("")
		telefono.set("")
		correo.set("")

	contacto = fetch_contacto(indice)
	editar_form = Toplevel(ventana)

	editar_form.title("Editar Contacto")
	editar_form.config(padx=10, pady=10)

	limpiar = Menu(editar_form)
	editar_form.config(menu=limpiar)

	editarsubmen = Menu(limpiar, tearoff=0)
	editarsubmen.add_command(label="Limpiar", command=limpiar_form)
	editarsubmen.add_separator()
	editarsubmen.add_command(label="Cancelar", command=editar_form.withdraw)

	limpiar.add_cascade(label="Editar", menu=editarsubmen)

	nombre = StringVar()
	telefono = StringVar()
	correo = StringVar()
	id = StringVar()

	nombre.set(contacto[1])
	telefono.set(contacto[2])
	correo.set(contacto[3])
	id.set(contacto[0])

	def save_contacto():
		contacto = (nombre.get(), telefono.get(), correo.get())
		conn = sqlite3.connect('contactos.db')
		c = conn.cursor()
		c.execute("INSERT INTO contactos (null,?,?,?)", contacto)
		conn.commit()
		conn.close()
		messagebox.showinfo('Contacto Actualizado', 'Se ha actualizado el contacto')

	Label(editar_form, textvariable=id).grid(row=0, column=0)
	Label(editar_form, text="Nombre:").grid(row=0, column=0)
	Entry(editar_form, justify="center", textvariable=nombre).grid(row=0, column=1)
	Label(editar_form, text="Teléfono:").grid(row=1, column=0)
	Entry(editar_form, justify="center", textvariable=telefono).grid(row=1, column=1)
	Label(editar_form, text="Correo:").grid(row=2, column=0)
	Entry(editar_form, justify="center", textvariable=correo).grid(row=2, column=1)

	Button(editar_form, text="Guardar", command=save_contacto).grid(row=3, column=0, columnspan=2)

def nuevo_contacto():

	def limpiar_form():
		nombre.set("")
		telefono.set("")
		correo.set("")

	def save_contacto():
		contacto = (nombre.get(), telefono.get(), correo.get())
		conexion = sqlite3.connect('contactos.db')
		cursor = conexion.cursor()
		cursor.execute("INSERT INTO contactos VALUES(null,?,?,?)", contacto)
		conexion.commit()
		conexion.close()
		limpiar_form()
		nuevo_form.withdraw()
		messagebox.showinfo('Exito', 'Se ha guardado el contacto')

	nuevo_form = Toplevel(ventana)
	# nuevo_form.geometry("300x300")
	nuevo_form.title("Nuevo Contacto")
	nuevo_form.config(padx=10, pady=10)

	limpiar = Menu(nuevo_form)
	nuevo_form.config(menu=limpiar)

	editarsubmen = Menu(limpiar, tearoff=0)
	editarsubmen.add_command(label="Limpiar", command=limpiar_form)
	editarsubmen.add_separator()
	editarsubmen.add_command(label="Cancelar", command=nuevo_form.withdraw)

	limpiar.add_cascade(label="Editar", menu=editarsubmen)

	nombre = StringVar()
	telefono = StringVar()
	correo = StringVar()


	Label(nuevo_form, text="Nombre:").grid(row=0, column=0)
	Entry(nuevo_form, justify="center", textvariable=nombre).grid(row=0, column=1)
	Label(nuevo_form, text="Teléfono:").grid(row=1, column=0)
	Entry(nuevo_form, justify="center", textvariable=telefono).grid(row=1, column=1)
	Label(nuevo_form, text="Correo:").grid(row=2, column=0)
	Entry(nuevo_form, justify="center", textvariable=correo).grid(row=2, column=1)

	Button(nuevo_form, text="Guardar", command=save_contacto).grid(row=3, column=0, columnspan=2)

	#formulario


# ventana
ventana = Tk()
ventana.title("Agenda")
ventana.geometry("800x500")
ventana.config(padx=25, pady=25)

## Menu

barra = Menu(ventana)
ventana.config(menu=barra)

archivomenu = Menu(barra, tearoff=0)
archivomenu.add_command(label="Nuevo Contacto", command=nuevo_contacto)
archivomenu.add_command(label="Exportar a txt", command=export_contactos)
archivomenu.add_separator()
archivomenu.add_command(label="Salir", command=ventana.quit)





ayudamenu = Menu(barra, tearoff=0)
ayudamenu.add_command(label="Ayuda")
ayudamenu.add_command(label="Acerca de")

barra.add_cascade(label='Archivo', menu=archivomenu)
barra.add_command(label="Actualizar", command=print_contactos)
barra.add_separator()
barra.add_cascade(label='Ayuda', menu=ayudamenu)

# Componentes
get_contactos()



#inicio de la app
ventana.mainloop()